import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html'
})
export class LinksComponent implements OnInit {
  @Input('book') book;
  ngOnInit(){}
  addLinkField() {
    this.book.link.push({type: 'epub', link: ''});
  }
}
