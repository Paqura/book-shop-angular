import { Component, OnInit } from '@angular/core';
import { BooksService } from '../../services/books.service';
import { Book } from '../../models/Book';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html'
})
export class PanelComponent implements OnInit {
  books: Book[];

  constructor(
    public booksService: BooksService
  ) { }

  ngOnInit() {
    this.booksService
      .getBooks()
      .subscribe(
        (books: Book[]) => this.books = books
      )
  }

  deleteCard(id: string) {
    this.booksService.deleteBook(id).subscribe((books: Book[]) => {
      this.books = books;
    });
  }
}
