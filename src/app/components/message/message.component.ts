import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html'
})
export class MessageComponent implements OnInit {
  @Input('text') text;
  @Input('role') role;
  @Input('type') type;
  @Input('error') error;

  constructor() { }

  ngOnInit() {

  }

  setText() {
    this.text = 'Error';
  }

  setRole() {
    return;
  }

  setType() {
    return;
  }

}
