import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { BooksService } from '../../services/books.service';
import { Book } from '../../models/Book';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html'
})
export class AddBookComponent implements OnInit {
  @ViewChild('form') form;

  book: Book = {
    id: '',
    name: '',
    author: '',
    description: '',
    link: [{type: 'pdf', link: ''}]
  };

  constructor(
    public booksService: BooksService,
    public router: Router
  ) { }

  ngOnInit() {}

  addBook() {
    this.booksService.addBook(this.book).subscribe(((newBook: Book) => {
      if(newBook) {
        this.router.navigate(['/panel']);
      }
    }))
  }

}
