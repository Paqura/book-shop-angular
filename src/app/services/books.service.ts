import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Book } from '../models/Book';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  books: Book[] = [
    {
      id: '0',
      author: 'Man',
      name: 'Выразительный Javascript',
      description: 'ES2018',
      link: [
        {
          type: 'epub',
          link: 'link'
        },
        {
          type: 'pdf',
          link: 'link'
        }
      ]
    }
  ]
  constructor() { }

  getBooks() {
    return of(this.books);
  }

  getBooksById(id: string) {
    const currentBook = this.books.find((book: Book) => book.id === id);
    return of(currentBook);
  }

  addBook(newBook: Book) {
    newBook.id = this.books.length.toString();
    this.books.unshift(newBook);
    return of(newBook);
  }

  editBook(editedBook: Book) {
    this.books = this.books.map((book: Book) => {
      if(book.id === editedBook.id) {
        return editedBook;
      }
      return book;
    });
    return of(editedBook);
  }

  deleteBook(id: string) {
    this.books = this.books.filter((book: Book) => book.id !== id);
    return of(this.books);
  }
}
