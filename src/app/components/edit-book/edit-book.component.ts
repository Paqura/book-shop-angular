import { Component, OnInit } from '@angular/core';
import { BooksService } from '../../services/books.service';
import { Book } from '../../models/Book';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html'
})
export class EditBookComponent implements OnInit {
  bookId: string;
  book: Book;

  constructor(
    public bookService: BooksService,
    public activatedRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
    this.bookId = this.activatedRoute.snapshot.params['id'];
    this.bookService.getBooksById(this.bookId).subscribe((book: Book) => this.book = book);
  }

  editBook() {
    const editedBook = Object.assign({}, this.book);

    this.bookService.editBook(editedBook).subscribe((updatedBook: Book) => {
      if(updatedBook) {
        this.router.navigate(['/panel']);
      }
    });
  }

}
