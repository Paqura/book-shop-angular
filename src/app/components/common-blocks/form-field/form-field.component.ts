import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html'
})
export class FormFieldComponent implements OnInit {
  @Input('propName') propName;
  @Input('state') state;
  ngOnInit() {}
}
