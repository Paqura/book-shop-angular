import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
  isLogin: boolean = false;

  constructor(
    public authService: AuthService,
    public router: Router
  ) {}
  ngOnInit() {
    this.authService.checkAuth().subscribe(authState => {
      if(authState) {
        this.isLogin = true;
      } else {
        this.isLogin = false;
      }
    })
  }

  logout() {
    this.authService.logout()
      .then(state => {
        this.router.navigate(['/login'])
      })
      .catch(err => console.log(err))
  }

}
