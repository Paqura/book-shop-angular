import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AngularFireAuth} from 'angularfire2/auth';
import { Observable, of } from 'rxjs';
import { tap, map, take } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private auth: AngularFireAuth,
    private router: Router
  ) {}

  canActivate(): Observable<boolean> | boolean {
      return this.auth.authState.pipe(
        take(1),
        map(user => {
          if(!user) {
            this.router.navigate(['/login']);
            return false;
          }
          return true;
        })
      )
  }
}
