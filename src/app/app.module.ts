import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PanelComponent } from './components/panel/panel.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { EditBookComponent } from './components/edit-book/edit-book.component';
import { AboutComponent } from './components/about/about.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import {FormsModule} from '@angular/forms';
import { LinksComponent } from './components/common-blocks/links/links.component';
import { SaveComponent } from './components/common-blocks/buttons/save/save.component';
import { FormFieldComponent } from './components/common-blocks/form-field/form-field.component';
import { LoginComponent } from './components/login/login.component';
import { BooksService } from './services/books.service';
import { AuthService } from './services/auth.service';
import { GeneratorService } from './services/generator.service';
import { AngularFireModule } from 'angularfire2';
import {environment} from '../environments/environment';
import { MessageComponent } from './components/message/message.component';
import { CoreModule } from './core/core.module';
import { AuthGuard } from './guard/auth.guard';
@NgModule({
  declarations: [
    AppComponent,
    PanelComponent,
    AddBookComponent,
    EditBookComponent,
    AboutComponent,
    NotFoundComponent,
    NavbarComponent,
    LinksComponent,
    SaveComponent,
    FormFieldComponent,
    LoginComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    CoreModule
  ],
  providers: [BooksService, GeneratorService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
